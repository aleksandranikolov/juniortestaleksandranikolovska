<?php

include_once 'header.php';
?>
<form name="product-form" action="index.php?page=create-product" method="post" id="product_form">

    <div class="container-fluid pl-5 pr-5 mt-5 mb-5 pl-5">
        <div class="row">
            <div class="col-lg-10">
                <h2 class="pl-5">Product Add</h2>
            </div>
            <div class="col-lg-2">
                <button type="submit" class="btn btn-info" name="btn-save">Save</button>
                <a href="index.php" class="btn btn-info">Cancel</a>
            </div>
        </div>
        <hr>

        <div class="container pl-5 pr-5 pb-5">
            <div class="form-group">
                <label for="sku">SKU</label>
                <input type="text" class="form-control" name="sku" id="sku">
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="Phone">Price $</label>
                <input type="number" class="form-control" name="price" id="price" step="0.01">
            </div>

            <div class="form-group">
                <label for="type">Type</label>
                <select class="form-control" name="type_id" id="productType">
                    <option value="" disabled selected hidden class="font-weight-bold">Type Switcher</option>
                    <option value="1">DVD-disc</option>
                    <option value="2">Book</option>
                    <option value="3">Furniture</option>
                </select>
            </div>

            <div class="form-group" id="size_mb">
                <label for="size">Please, provide size in MB</label>
                <input type="number" class="form-control" name="size_mb" id="size" step="0.01">
            </div>

            <div class="form-group" id="weight_div">
                <label for="weight">Please provide weight in KG</label>
                <input type="number" class="form-control" name="weight" id="weight" step="0.01">
            </div>
            <div class="form-group" id="dimensions">
                <label for="dimension">Please provide dimensions</label>
                <p>Heigth CM:</p><input type="number" class="form-control" name="heigth_cm" id="height" step="0.01">
               <p> Width CM:</p><input type="number" class="form-control mt-3" name="width_cm" id="width" step="0.01">
                <p>Length CM:</p><input type="number" class="form-control mt-3" name="length_cm" id="length" step="0.01">
            </div>

        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {

        $('#product_form').submit(function(e) {
            let errors_ind = false;
            $(".error").remove();
            console.log( $('#weight').val())

            if ($('#sku').val().length < 1) {
                errors_ind = true;
                $('#sku').after('<span class="error">*SKU field is required</span>');
            }

            if ($('#name').val().length < 1) {
                errors_ind = true;
                $('#name').after('<span class="error">*Name field is required</span>');
            }

            if ($('#price').val().length < 1) {
                errors_ind = true;
                $('#price').after('<span class="error">*Price field is required</span>');
            }

            console.log($('#productType').val())
            if ($('#productType').val() == null) {
                errors_ind = true;
                $('#productType').after('<span class="error">*Type field is required</span>');
            }

            if ($('#productType').val() == '1' && $('#size').val() == '') {
                errors_ind = true;
                $('#size').after('<span class="error">*Size field is required</span>');

            } 
            
            if ($('#productType').val() == '2' && $('#weight').val() == '') {
                errors_ind = true;
                $('#weight').after('<span class="error">*Weight field is required</span>');

            } 


            if ($('#productType').val() == '3' && $('#height').val() == '') {
                errors_ind = true;
                $('#height').after('<p class="error"> *Heigth field is required</p>');

            } 

            if ($('#productType').val() == '3' && $('#width').val() == '') {
                errors_ind = true;
                $('#width').after('<span class="error">*Width field is required</span>');

            } 

            if ($('#productType').val() == '3' && $('#length').val() == '') {
                errors_ind = true;
                $('#length').after('<span class="error">*Length field is required</span>');

            } 

            if (errors_ind) {
                e.preventDefault();
            }
        });


        $("#size_mb").hide();
        $("#weight_div").hide();
        $("#dimensions").hide();
        $('#productType').on('change', function() {
            if (this.value == '1') {
                $("#size_mb").show();
                $("#weight_div").hide();
                $("#dimensions").hide();
            } else if (this.value == '2') {
                $("#weight_div").show();
                $("#size_mb").hide();
                $("#dimensions").hide();
            } else {
                $("#dimensions").show();
                $("#weight_div").hide();
                $("#size_mb").hide();
            }
        });
    });
</script>



<?php include_once 'footer.php'; ?>