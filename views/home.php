<?php
include_once 'header.php';
?>

<div class="navbar navbar-default pt-5 navbar-static-top" >
    <div class="container">
        <div class="col-9">
            <h2>Product list</h2>
        </div>
        <div class="col-3">
            <a class="navbar-brand btn btn-info text-white" href="/Test/index.php?page=add-product">Add</a>
            <a class="navbar-brand btn btn-info text-white" id="delete-product-btn">MASS DELETE</a>
        </div>
    </div>
</div>
<hr>

<div class="container">
    <div class="row">
        <?php foreach ($products as $row) { ?>
            <div class="col-3">
                <div class="card mt-3">
                    <div class="card-body">
                        <input type="checkbox" class="delete-checkbox" name="<?php echo $row['id']; ?>">
                        <div class="text-center">
                            <h5 class="card-title d-block"><?php print($row['sku']); ?></h5>
                            <p class="card-text"><?php print($row['name']); ?></p>
                            <p class="card-text">$<?php print($row['price']); ?></p>

                            <?php if (($row['type_id']) === '1') : ?>
                                <p class="card-text">Size: <?php echo ($row['size_mb']) ?> MB</p>
                            <?php elseif (($row['type_id']) == 2) : ?>
                                <p class="card-text">Weight: <?php echo ($row['weight']) ?>KG</p>
                            <?php elseif (($row['type_id']) === '3') : ?>
                                <span>Dimension: <?php echo ($row['heigth_cm']) ?> x</span>
                                <span><?php echo ($row['width_cm']) ?> x</span>
                                <span><?php echo ($row['length_cm']) ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        let deleteIds = [];

        $('#delete-product-btn').click(function() {
            $('.delete-checkbox').each(function() {
                if ($(this).is(":checked")) {
                    deleteIds.push($(this).attr('name'));
                }
            });

            // call to delete the products
            window.location.href = "index.php?page=delete-products&product_ids=" + deleteIds.join();
            console.log(deleteIds);
            deleteIds = [];
        });
    });
</script>

<?php include_once 'footer.php'; ?>