<?php

include_once 'Models/Product.php';

class ProductController
{
    public function addProductPage()
    {
        include_once 'views/add_product.php';
    }

    public function createProduct()
    {
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $type_id = $_POST['type_id'];
        $size_mb = $_POST['size_mb'];
        $weight = $_POST['weight'];
        $heigth_cm = $_POST['heigth_cm'];
        $width_cm = $_POST['width_cm'];
        $length_cm = $_POST['length_cm'];

        $productOne = new Product();
        $productOne->createProduct($sku, $name, $price, $type_id, $size_mb, $weight, $heigth_cm, $width_cm, $length_cm);

        header("Location: index.php"); 
        exit();
    }

    public function deleteProducts() {

        $product_ids_string = $_GET['product_ids'];
        // $product_ids = explode(',', $product_ids_string);

        $productOne = new Product();
        $productOne->deleteProducts($product_ids_string);


        header("Location: index.php"); 
        exit();
    }
}
